package com.reto.retodz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetodzApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetodzApplication.class, args);
	}

}
